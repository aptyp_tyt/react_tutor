import React from 'react'
import ScrollDispatcher from 'dispatchers/ScrollDispatcher'

export const Anchor = React.createClass({
  propTypes: {
    post: React.PropTypes.object.isRequired
  },

  _scroll (e) {
    ScrollDispatcher.scroll('post_' + this.props.post.id)
  },

  render () {
    return (
      <li onClick={this._scroll}>{this.props.post.title}</li>
    )
  }
})

export default Anchor
