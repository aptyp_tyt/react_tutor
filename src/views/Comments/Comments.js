import React from 'react'
import styles from './Comments.scss'
import Comment from 'views/Comment'

export const CommentList = React.createClass({
  propTypes: {
    post: React.PropTypes.object.isRequired
  },

  getInitialState () {
    return {
      user: '',
      message: ''
    }
  },

  _changeName (e) {
    this.setState({user: e.currentTarget.value})
  },

  _changeMessage (e) {
    this.setState({message: e.currentTarget.value})
  },

  _addComment () {
    this.props.post.addComment({user: this.state.user, message: this.state.message})
    this.setState({ user: '', message: '' })
  },

  renderForm () {
    return (
      <div className={styles.commentform}>
        <input type="text" placeholder="Type your name here..." value={this.state.user} onChange={this._changeName} />
        <textarea placeholder="Type your comment here..." value={this.state.message} onChange={this._changeMessage} />
        <button className="btn btn-default" onClick={this._addComment}>Submit</button>
      </div>
    )
  },

  render () {
    let comments = this.props.post.comments

    return (
      <div className={styles.comment_list}>
        <span>Comments:</span>
        {comments.list.map((comment, i) => {
          return <Comment key={i} model={comment} />
        })}
        {this.renderForm()}
      </div>
    )
  }
})

export default CommentList
