import React from 'react'
import styles from './Comment.scss'

export const CommentList = React.createClass({
  propTypes: {
    model: React.PropTypes.object.isRequired
  },

  render () {
    return (
      <div className={styles.comment}>
        <author>{this.props.model.user}:</author>
        <message>{this.props.model.message}</message>
      </div>
    )
  }
})

export default CommentList
