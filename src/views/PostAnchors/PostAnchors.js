import React from 'react'
import store from 'store/PostList'
import Anchor from 'views/Anchor'

export const PostAnchors = React.createClass({
  _listener: null,

  componentDidMount () {
    this._listener = store.addListener('change', () => {
      this.forceUpdate()
    }
  )
  },

  componentWillUnmount () {
    this._listener.remove()
  },

  render () {
    return (
      <ul>
        {store.list.map((post, i) => {
          return <Anchor key={i} post={post} />
        })
      }
      </ul>
    )
  }
})

export default PostAnchors
