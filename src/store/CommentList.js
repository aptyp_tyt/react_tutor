import _ from 'lodash'
import {EventEmitter} from 'fbemitter'
import Comment from 'models/Comment'

var CommentList = function (post, comments = []) {
  this.post = post
  this.list = comments
}

CommentList.prototype = new EventEmitter()

_.extend(CommentList.prototype, {
  addComment ({user, message}) {
    let comment = new Comment({user, message})
    this.list.push(comment)
    this.update()
  },

  update () {
    this.emit('change')
  }
})

export default CommentList
