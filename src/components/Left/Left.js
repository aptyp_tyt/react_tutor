import React from 'react'
import classes from './Left.scss'
import PostAnchors from 'views/PostAnchors'

export const Left = React.createClass({
  render () {
    return (
      <div className={classes.left}>
        <PostAnchors />
      </div>
    )
  }
})

export default Left
