/*
  Inline labels editor

  Usage example:
  const Page = React.createClass({
    getInitialState () {
      return {
        value: 'Click to change...'
      }
    },

    _change (value) {
      this.setState({value})
    },

    render () {
      return (<LabelEditor value={this.state.value} onChange={this._change} />)
    }
  })

  Props:
    value: value
    onChange: will trigger when data changed
*/

import React from 'react'
import styles from './LabelEditor.scss'

export const LabelEditor = React.createClass({
  propTypes: {
    value: React.PropTypes.string.isRequired,
    onChange: React.PropTypes.func.isRequired
  },

  getInitialState () {
    return {
      edit: false,
      value: this.props.value
    }
  },

  componentDidUpdate () {
    if (this.state.edit) {
      this.refs.editor.focus()
    }
  },

  _edit () {
    this.setState({edit: true})
  },

  _blur () {
    this.props.onChange(this.state.value)
    this.setState({edit: false})
  },

  _change () {
    this.setState({value: this.refs.editor.value})
  },

  _keyUp (e) {
    if (e.keyCode === 27) {
      this.setState({edit: false, value: this.props.value})
    }
  },

  renderText (value) {
    return (
      <span onClick={this._edit} className={styles.title}>
        <i className="fa fa-pencil-square-o" aria-hidden="true" />
        {value || this.state.value}
      </span>
    )
  },

  renderEdit (value) {
    return (
      <textarea value={value || this.state.value} onChange={this._change} onBlur={this._blur} ref="editor"
        className={styles.labelEdit} onKeyUp={this._keyUp} />
      )
  },

  render () {
    return (this.state.edit ? this.renderEdit() : this.renderText())
  }
})

export default LabelEditor
