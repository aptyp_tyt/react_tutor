import $ from 'jquery'
var {EventEmitter} = require('fbemitter')
var dispatcher = new EventEmitter()

dispatcher.scroll = function (hash) {
  $('html,body').animate({scrollTop: $(`[name="${hash}"]`).offset().top - 10}, 200)
}

export default dispatcher
