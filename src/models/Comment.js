var count = 1

export const Comment = function (attrs = {}) {
  this.user = attrs.user
  this.message = attrs.message
  this.id = count
  count++
}

export default Comment
